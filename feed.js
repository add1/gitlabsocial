"use strict";

class Storage{

    setItem(key, value) {
        window.localStorage.removeItem(key);
        window.localStorage.setItem(key, value);
    }

    getItem(key) {
        var value;
        try {
            value = window.localStorage.getItem(key);
        }catch(e) {
            value = "null";
        }
        return value;
    }

}

class UserService{

    constructor(userRepository){
        this.repository = userRepository;
    }
    
    isFollowingUser(username){
        return this.repository.getUsers().indexOf(username) >= 0;
    }

}

class UserRepository{

    constructor(storage){
        this.storage = storage;
    }

    addUser(userToFollow){
        var following = this.getUsers();
        if(!following)
            following = [];
        following.push(userToFollow);
        this.storage.setItem('following', JSON.stringify(following));
    }

    removeUser(userToUnfollow){
        var following = this.getUsers();
        following.splice(following.indexOf(userToUnfollow), 1)
        this.storage.setItem('following', JSON.stringify(following));
    }

    getUsers(){
        return JSON.parse(this.storage.getItem('following'));
    }

}

class User{
    constructor(username, name, link, avatar){
        this.username = username;
        this.name = name;
        this.link = link;
        this.avatar = avatar;
    }
}

class FeedItem{
    constructor(link, name, userlink, avatar, title, summary, timestamp){
        this.link = link;
        this.name = name;
        this.userlink = userlink;
        this.avatar = avatar;
        this.title = title;
        this.summary = summary;
        this.timestamp = timestamp;
    }
}

class RenderHtml{
    render() {
        throw "The render method should be overwritten";
    }

}

class FeedItemParser{
    constructor(jQueryData, entry){
        this.jQueryData = jQueryData
        this.entry = entry
    }
    parse(){
        var username = this.jQueryData.find('entry username').eq(this.entry).text();
        var name = this.jQueryData.find('entry name').eq(this.entry).text();
        var userlink = '/' + username;
        var title = this.jQueryData.find('entry title').eq(this.entry).text();
        var timestamp = new Date(this.jQueryData.find('entry updated').eq(this.entry).text());
        var avatar = this.jQueryData.find('entry media\\:thumbnail').eq(this.entry).attr('url');
        var link = this.jQueryData.find('entry link').eq(this.entry).attr('href');
        var summary = $.trim(this.jQueryData.find('entry summary').eq(this.entry).text()).slice(0,140);
        return new FeedItem(link, name, userlink, avatar, title, summary, timestamp);
    }
}

class FeedItemHtml extends RenderHtml{
    constructor(feedItem){
        super();
        this.feedItem = feedItem;
    }

    render(){
        return '<div class="row" style="margin-top: 10px;">' +
                 '<div class="col-lg-12">' +
                    '<div class="blank-state" style="padding: 20px; text-align:left">' +
                        '<div class="blank-state-icon">' +
                            '<a href="'+ this.feedItem.userlink +'">' +
                              '<img width="40" height="40" src="' + this.feedItem.avatar + '"/> ' +
                            '</a>' +
                        '</div>' +
                        '<div class="blank-state-body">' +
                            '<a href="'+ this.feedItem.link +'">' +
                            '<h3 class="blank-state-title" style="font-size: 14px;">' +
                                this.feedItem.title + ' ('+ this.feedItem.timestamp.toUTCString() +')' + 
                            '</h3>' +
                            '<p class="blank-state-text">' +
                                this.feedItem.summary +
                            '</p>' +
                            '</a>' +
                        '</div>' +
                    '</div>' +
                '</div>'+
            '</div>'
    }
}

class Feed extends Array{
    constructor(name, ...items){
        super(...items);
        this.name = name;
    }
}

class FeedHtml extends RenderHtml{
    constructor(feed){
        super();
        this.feed = feed;
    }
    render(){
        return this.feed.map(function(elem, index){
            if(index < 18)
                return (new FeedItemHtml(elem)).render();
        }).join("");
    }
}

class FollowingHtml extends RenderHtml{
    constructor(feed){
        super();
        this.feed = feed;
    }
    render(){
        return this.feed.map(function(elem, index){
            return '<div class=" d-table table-fixed col-12 width-full py-4 border-bottom border-gray-light">'+
                    '<a href="' + elem.userlink +'">'+
                    '<img width="40" height="40" src="' + elem.avatar + '"/> '+ elem.name +
                    '</a>'+
                    '</div>';
        }).unique().join("");
    }
}

class PageService{
    static isValidPage(){
        return $('body').data('page') == "root:index" || $('body').data('page') == "dashboard:projects:index"
    }
}

function sortTimestamp(dateA, dateB){  
    return new Date(dateA.timestamp) > new Date(dateB.timestamp);
}

Array.prototype.unique = function() {
  return this.filter(function (value, index, self) { 
    return self.indexOf(value) === index;
  });
}

class App{
    static run(){
        if(PageService.isValidPage()){

            var feedObj = new Feed('current feed');
            var ajaxQueue = [];

            userRepository.getUsers().map(function(userId){
                var ajaxPromises = $.get('https://gitlab.com/'+ userId +'.atom', function(data){
                    $(data).find('entry').each(function(entry){
                        var feedParser = new FeedItemParser($(data), entry);
                        feedObj.push(feedParser.parse());
                    })
                    
                })
                ajaxQueue.push(ajaxPromises);
            })

            $.when.apply(null, ajaxQueue).done(function(){
                feedObj.sort(sortTimestamp)
                var feedObjHtml = new FeedHtml(feedObj);
                var followingHtml = new FollowingHtml(feedObj);
                $(".feed").html(feedObjHtml.render());
                $(".following").html(followingHtml.render());
            })

            //without projects
            if($(".container.section-body").length > 0){
                $(".container.section-body").prepend(
                    '<div class="row">'+
                        '<div class="col-lg-6">' + 
                            '<h2 class="blank-state-welcome-title">' + 
                                'Browser Activities' + 
                            '</h2>' + 
                            '<div class="feed">' + 
                                'Loading...' +
                            '</div>' + 
                        '</div>'+
                         '<div class="col-lg-6">' + 
                            '<h2 class="blank-state-welcome-title">' + 
                                'Following' + 
                            '</h2>' + 
                            '<div class="following" style="margin-left: 10px;">' + 'Loading...' +
                            '</div>' + 
                        '</div>'+
                    '</div>'
                );
            }else{
                $("#content-body").parent().addClass('row')
                $("#content-body").addClass('col-lg-6')
                $("#content-body").after(
                    '<div class="col-lg-6">' + 
                        '<ul class="nav-links scrolling-tabs mobile-separator nav nav-tabs is-initialized">' +
                            '<li class="active activities-li"><a class="activities-header shortcuts-activity" data-placement="right" href="#">Browser Activities</a></li>' +
                            '<li class="following-li"><a class="following-header " data-placement="right" href="#">Following</a></li>' +
                        '</ul>' +
                        '<div class="feed">' + 
                            'Loading...' +
                        '</div>' + 
                        '<div class="following" style="display:none; margin-left: 10px;">' + 'Loading...' +
                        '</div>' +
                    '</div>'
                );
                $(".activities-header").on("click",function(){
                    $(".feed").show()
                    $(".following").hide()
                    $(".activities-li").addClass('active')
                    $(".following-li").removeClass('active')
                })
                $(".following-header").on("click",function(){
                    $(".feed").hide()
                    $(".following").show()
                    $(".activities-li").removeClass('active')
                    $(".following-li").addClass('active')
                })
            }
        }

        function changeFollowButton(){
            var username = $('.js-activity-tab a').attr('href').slice(1);
            $(".btn-follow-social").off("click")
            if(userService.isFollowingUser(username)){
                $(".btn-follow-social").text('Unfollow')
                $(".btn-follow-social").on("click", function(){
                    userRepository.removeUser(username)
                    changeFollowButton();
                })
            }else{
                $(".btn-follow-social").text('Follow')
                $(".btn-follow-social").on("click", function(){
                    userRepository.addUser(username)
                    changeFollowButton();
                })
            }
        }

        if($('body').data('page') == "users:show"){
            $(".cover-controls").prepend('<a class="btn btn-follow-social" title="" data-toggle="tooltip" data-placement="bottom" data-container="body" href="#" data-original-title="Follow"></a>');
            changeFollowButton()
        }

    }
}

var userRepository = new UserRepository(new Storage())
var userService = new UserService(userRepository)
App.run();



